import DarkMode from "../DarkMode/DarkMode";
import RestContext from "../../context/RestContext";
import { useContext } from "react";

function Header() {
  const { darkModeColor } = useContext(RestContext);
  return (
    <div className={`header-container  ${darkModeColor ? "dark-elements" : "light-elements"}`}>
      <p className="where">Where in the world?</p>
      <DarkMode />
    </div>
  );
}

export default Header;
